// Soal 1

function jumlah_kata(kalimat_1) {
    var kata = kalimat_1.split(" ");
    return kata.length
}

function jumlah_kata(kalimat_2){
    var kata = kalimat_2.split(" ");
    return kata.length
}

function jumlah_kata(kalimat_3){
    var kata = kalimat_3.split(" ");
    return kata.length
}

var kalimat_1 = "Halo nama saya Siti Fatimah"
var kalimat_2 = "Saya Fatimah"
var kalimat_3 = "Saya Siti Fatimah"


console.log(jumlah_kata(kalimat_1)) // 5
console.log(jumlah_kata(kalimat_2)) // 2
console.log(jumlah_kata(kalimat_3)) // 3

// Soal 2 

var tanggal = 29;
var bulan = 2;
var tahun = 2024;

function next_date(tanggal,bulan,tahun){
    if (bulan == 1) {
        var tanggal = tanggal + 1
        var bulan = 'januari'
        if (tanggal > 31){
            tanggal = '1'
            bulan = 'Februari'
            };
        console.log(tanggal + ' ' + bulan + ' ' + tahun)}
        else if (bulan == 2){
            var tanggal = tanggal + 1
            var bulan = 'Februari'
            if (tahun % 4 != 0 && tanggal>28){
                tanggal = '1'
                bulan = 'Maret'
            }
            else if (tahun % 4 == 0 && tanggal == 28 )  {
                tanggal = '29'
                bulan = 'Februari'
            }
            else if (tahun % 4 == 0 && tanggal > 29 )  {
                tanggal = '1'
                bulan = 'Maret'
            }
            console.log(tanggal + ' ' + bulan + ' ' + tahun)
        }
        else if (bulan == 3){
            var tanggal = tanggal + 1
            var bulan = 'Maret'
            if (tanggal > 31){
                tanggal = '1'
                bulan = 'April'
            };
            console.log(tanggal + ' ' + bulan + ' ' + tahun)
        }
        else if (bulan == 4){
            var tanggal = tanggal + 1
            var bulan = 'April'
            if (tanggal > 30){
                tanggal = '1'
                bulan = 'Mei'
            };
            console.log(tanggal + ' ' + bulan + ' ' + tahun)
        }
        else if (bulan == 5){
            var tanggal = tanggal + 1
            var bulan = 'Mei'
            if (tanggal > 31){
                tanggal = '1'
                bulan = 'Juni'
            };
            console.log(tanggal + ' ' + bulan + ' ' +tahun)
        }
        else if (bulan == 6){
            var tanggal = tanggal + 1
            var bulan = 'Juni'
            if (tanggal > 30){
                tanggal = '1'
                bulan = 'Juli'
            };
            console.log(tanggal + ' ' + bulan + ' ' + tahun)
        }
        else if (bulan == 7){
            var tanggal = tanggal + 1
            var bulan = 'Juli'
            if (tanggal > 31){
                tanggal = '1'
                bulan = 'Agustus'
            };
            console.log(tanggal + ' ' + bulan + ' ' + tahun)
        }
        else if (bulan == 8){
            var tanggal = tanggal + 1
            var bulan = 'Agustus'
            if (tanggal > 31){
                tanggal = '1'
                bulan = 'September'
            };
            console.log(tanggal + ' ' + bulan + ' ' +tahun)
        }
        else if (bulan == 9){
            var tanggal = tanggal + 1
            var bulan = 'September'
            if (tanggal > 30){
                tanggal = '1'
                bulan = 'Oktober'
            };
            console.log(tanggal + ' ' + bulan + ' ' + tahun)
        }
        else if (bulan == 10){
            var tanggal = tanggal + 1
            var bulan = 'Oktober'
            if (tanggal > 31){
                tanggal = '1'
                bulan = 'November'
            };
            console.log(tanggal + ' ' + bulan + ' ' + tahun)
        }
        else if (bulan == 11){
            var tanggal = tanggal + 1
            var bulan = 'November'
            if (tanggal > 30){
                tanggal = '1'
                bulan = 'Desember'
            };
            console.log(tanggal + ' ' + bulan + ' ' + tahun)
        }
        else if (bulan == 12){
            var tanggal = tanggal + 1
            var bulan = 'Desember'
            if (tanggal > 31){
                tanggal = '1'
                bulan = 'Januari'
                tahun = tahun + 1
            };
            console.log(tanggal + ' ' + bulan + ' ' + tahun)
        }
        else{
            console.log('Masukan input date dengan benar')
        }
}

console.log(next_date(tanggal,bulan,tahun));