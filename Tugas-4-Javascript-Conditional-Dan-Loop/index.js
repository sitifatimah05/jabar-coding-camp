//name : Siti Fatimah

// Soal 1

var nilai = 80;

if ( nilai >= 85 ) {
    console.log("A")
} else if ( nilai >= 75 && nilai < 85 ) {
    console.log("B")
} else if ( nilai >= 65 && nilai < 75 ) {
    console.log("C")
} else if ( nilai >= 55 && nilai < 65 ) {
    console.log("D")
} else {
    console.log("E")
}

// Soal 2

var tanggal = 20;
var bulan = 5;
var tahun = 2000;
switch(bulan) {
    case 1: {console.log(tanggal + ' ' + 'Januari' + ' ' + tahun ); break;}
    case 2: {console.log(tanggal + ' ' + 'Februari' + ' ' + tahun ); break;}
    case 3: {console.log(tanggal + ' ' + 'Maret' + ' ' + tahun ); break;}
    case 4: {console.log(tanggal + ' ' + 'April' + ' ' + tahun ); break;}
    case 5: {console.log(tanggal + ' ' + 'Mei' + ' ' + tahun ); break;}
    case 6: {console.log(tanggal + ' ' + 'Juni' + ' ' + tahun ); break;}
    case 7: {console.log(tanggal + ' ' + 'Juli' + ' ' + tahun ); break;}
    case 8: {console.log(tanggal + ' ' + 'Agustus' + ' ' + tahun ); break;}
    case 9: {console.log(tanggal + ' ' + 'September' + ' ' + tahun ); break;}
    case 10: {console.log(tanggal + ' ' + 'Oktober' + ' ' + tahun ); break;}
    case 11: {console.log(tanggal + ' ' + 'November' + ' ' + tahun ); break;}
    case 12: {console.log(tanggal + ' ' + 'Desember' + ' ' + tahun ); break;}
    default: {console.log('Bulan Tidak terdaftar')}
}


// Soal 3

var a = '';
var n= 10;

for (var i=1; i<= n; i++){
    for (var j = 1; j<=i; j++){
        a += '#';
    }
    a += '\n';
}
console.log(a);



// Soal 4

var m= 10;

for (var i=1; i<= m; i++){
    console.log(i + '- I love Programming');
    if (i %3 === 0){
        console.log("===");
    }

}