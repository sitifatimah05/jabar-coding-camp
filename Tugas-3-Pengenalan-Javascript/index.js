//name : Siti Fatimah

// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// Jawaban Soal 1
var ketiga = pertama.substr(0,5);
var keempat = pertama.substr(12,7);
var kelima = kedua.substr(0,8);
var keenam = kedua.substr(8,10)
var upper = keenam.toUpperCase();
console.log(ketiga.concat(keempat,kelima,upper)); //saya senang belajar JAVASCRIPT


// Soal 2
var kata1 = "10";
var kata2 = "2";
var kata3 = "4";
var kata4 = "6";

//Jawaban Soal 2

var strInt_1 = parseInt(kata1);
var strInt_2 = parseInt(kata2);
var strInt_3 = parseInt(kata3);
var strInt_4 = parseInt(kata4);
var jumlah = strInt_1 + strInt_2 * strInt_3 + strInt_4 ; //24
console.log(jumlah)

//Soal 3
var kalimat = 'wah javascript itu keren sekali'; 

//Jawaban Soal 3
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4, 11); // do your own! 
var kataKetiga = kalimat.substr(15, 3 ); // do your own! 
var kataKeempat = kalimat.substr(19, 5 ); // do your own! 
var kataKelima = kalimat.substring(25); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali
