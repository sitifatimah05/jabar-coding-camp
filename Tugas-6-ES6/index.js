// Soal 1

//Panjang*lebar = ;uas
//keliling = 2*panjang (lebar+lebar)

let p = 10
let l = 5

const LuasFunction = () => {
    luas = p*l
    return luas
}

const KelilingFunction = () => {
    keliling = 2 * p *(p+l)
    return keliling
}

// panggil Function
console.log(LuasFunction());
console.log(KelilingFunction());

// Soal 2

const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 
  